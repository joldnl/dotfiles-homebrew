#!/bin/bash

# ------------------------------------
# Install Homwbrew
# ------------------------------------

# Get Homebrew core
echo ""
echo "-------------------------------------------"
echo "| Install Homebrew ...                    |"
echo "-------------------------------------------"
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
echo "Install Homebrew ..."
echo "Done! ..."
echo ""



# Clone Homebrew Dotfiles Repo
echo ""
echo "-------------------------------------------"
echo "| Clone Homebrew Dotfiles Repo            |"
echo "-------------------------------------------"

git clone --depth=1 --branch=master https://joldnl@bitbucket.org/joldinteractive/dotfiles-homebrew.git
cd dotfiles-homebrew rm -rf !$/.git
cd ../
echo "Done! ..."



# Install and activate atom packages from packages.txt
echo ""
echo "-------------------------------------------"
echo "| Tapping repository sources ...          |"
echo "-------------------------------------------"
echo "Tapping repository sources ..."

cd dotfiles-homebrew
input="taps.txt"
while IFS= read -r var
do
    brew tap $var
done < "$input"
cd ../
echo "Done!"
echo ""




# Install and activate atom packages from packages.txt
echo ""
echo "-------------------------------------------"
echo "| Install Packages from formulae.txt ...  |"
echo "-------------------------------------------"
echo "Installing Homebrew packages ..."

cd dotfiles-homebrew
input="formulae.txt"
while IFS= read -r var
do
    brew install $var
done < "$input"
cd ../

echo "Done!"
echo ""
echo "-------------------------------------------"
echo "| Homebrew Installation Completed!        |"
echo "-------------------------------------------"
echo ""
echo "Verything Homebrew done ;)"
echo ""
